
import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2 as QControls
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Ubuntu.Content 1.3

import "Pages"
import "helpers"
import "Jslibs/nextCloudAPI.js" as NextcloudAPI

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'darkeye.ursses'
    automaticOrientation: true
	anchorToKeyboard: true

    width: units.gu(45)
    height: units.gu(75)
	
	theme.name: ""
    backgroundColor: theme.palette.normal.background
	property var urls :  [];
	
	onUrlsChanged : {
		console.log("onUrlsChanged")
		mainFeed.updateFeed();
		appSettings.sync();
	}
	
	Settings {
		id:appSettings
		property bool showDescInsteadOfWebPage: false
		property bool showSections: false
		property string mainFeedSectionField : "channel"
		property alias urls: root.urls
		property var bookedmarked : []
		property int itemsToLoadPerChannel : 5
		property int mainFeedSortAsc :Qt.AscendingOrder
		property string mainFeedSortField : "updated"
		property real webBrowserDefaultZoom : 1.0
		property int updateFeedEveryXMinutes : 15
		property var nextCloudCreds :{"host":"","user":"","pass":""}
	}
	
	AdaptivePageLayout {
		id:mainLayout
		anchors {
			fill:parent
		}
		primaryPage : MainFeedPage {
			id:mainFeed
				model: root.urls
				header: PageHeader {
					id: header
					title: i18n.tr('Rsses')
					trailingActionBar  { 
						numberOfSlots: 3
						actions : [
							Action {
								text : i18n.tr("Add Feed")
								iconName : "list-add"
								onTriggered : {
									mainLayout.addPageToNextColumn(mainLayout.primaryPage, Qt.resolvedUrl("Pages/AddRssPage.qml"),{})
								}
							},
							Action {
								text : i18n.tr("Settings")
								iconName : "settings"
								onTriggered : {
									mainLayout.addPageToCurrentColumn(mainLayout.primaryPage, Qt.resolvedUrl("Pages/SettingsPage.qml"),{})
								}
							},
							Action {
								text : i18n.tr("Toggle Sort")
								iconName : "sort-listitem"
								onTriggered : {
									appSettings.mainFeedSortAsc = appSettings.mainFeedSortAsc == Qt.AscendingOrder ? Qt.DescendingOrder : Qt.AscendingOrder;
								}
							}
						]
					}
				}
		}
	}
	
	// =============== Sync Next Cloud Feeds ===============
	Timer {
		id:syncNextCloudTimer
		running: appSettings.nextCloudCreds.accountId !== undefined && appSettings.nextCloudCreds.encodedCreds !== undefined &&
				 appSettings.nextCloudCreds.accountId && appSettings.nextCloudCreds.encodedCreds
		interval: 30 * 6000
		triggeredOnStart: true
		repeat: true
		onTriggered: {
			NextcloudAPI.getFeeds(appSettings.nextCloudCreds,function(feedsResult) {
				console.log(JSON.stringify(feedsResult));
				if(feedsResult && feedsResult.feeds) {
					for(var i in feedsResult.feeds) {
						if( feedsResult.feeds[i] && appSettings.urls.indexOf(feedsResult.feeds[i].url) < 0)
							appSettings.urls.push(feedsResult.feeds[i].url);
					}
				}
			});
		}
	}
	
	//==================================  Sharing is caring ===========================
	property var urlMapping: {
		"^https?://" : function(url) {
			//if(pageStack && pageStack.currentPage && pageStack.currentPage.folderModel) {
				//pageStack.currentPage.folderModel.goTo(url);
			//}
			console.log("handling http:// for:"+url);
			for(var i in root.urls) {
				if(url == root.urls[i]) {
					console.log(""+url+ " Allredy exists");
					return;
				}
			}
			root.urls[""+Object.keys(appSettings.urls).length] = url;
		}
	}

	DispatcherHandler {
			id:dispatcherHandler
	}

	Component.onCompleted:  {
		if(args) {
			console.log("onCompleted args:" + JSON.stringify(args));
			dispatcherHandler.handleOnCompleted(args);
		}
	}
	
	
	Connections {
		target: ContentHub
		onShareRequested: {
			if ( transfer.contentType === ContentType.Links ) {
					for ( var i = 0; i < transfer.items.length; i++ ) {
					if (String(transfer.items[i].url).length > 0 ) {
						var url = transfer.items[i].url;
						for(var i in root.urls) {
						if(url == root.urls[i]) {
							console.log(""+url+ " Allredy exists");
							return;
						}
					}
					root.urls[""+Object.keys(appSettings.urls).length] = url;
					console.log("Added Feed : " + url);
					}
				}
			}
		}
	}
}

/*
 * Copyright (C) 2021  Eran DarkEye Uzan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * darkeye.ursses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
