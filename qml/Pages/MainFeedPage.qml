
import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2 as QControls
import QtQuick.XmlListModel 2.0
import QtQml.Models 2.2

import "../Components"
import "../Components/UI"
import "../Jslibs/rssAPI.js" as RssAPI


Page {
	id:_mainFeed
	
	property var model : []
	property var lastRefresh: 0;
	property var refreshing:  false;
	
	header: PageHeader {
		id:header
	}
	
	Component.onCompleted : {
		_mainFeed.updateFeed();
	}
	
	Component {
		id:channelComponent
		RssChannel {
		}
	}
	
	Component {
		id:channelItemsComponent
		RssChannelItems {
		}
	}

	
	function loadChannelItems(channelData) {
		var channelItems = channelItemsComponent.createObject(null,{});
		if(channelData['isAtom']) {
			channelItems.namespaceDeclarations = "declare default element namespace 'http://www.w3.org/2005/Atom';";
		}
		channelItems.statusChanged.connect(function() { 
			if (channelItems.status == XmlListModel.Ready) {
				for(var i=0; i < channelItems.count && i < appSettings.itemsToLoadPerChannel; i++) {
					var item = channelItems.get(i);
					//console.log("channel items :" + JSON.stringify(item));
					item["chImageUrl"] = channelData["imageUrl"];
					item["channel"] = channelData["titleText"];
					item["itemData"] =  JSON.parse(JSON.stringify(item));
					feedList.model.append(item);
				}
			}
		});
		channelItems.source = channelData["feedUrl"];
	}

	function updateFeed() {
		if( !_mainFeed.model || _mainFeed.model.length == 0  ||  Date.now() - _mainFeed.lastRefresh < 10000 &&  _mainFeed.refreshing ) {
			console.log(Date.now(),  _mainFeed.lastRefresh)
			return;
		}
		
		console.log("updating the feeds..");
		_mainFeed.lastRefresh = Date.now();
		_mainFeed.refreshing = true;
		feedList.model.clear();
		var channels = {}
		for(var i in _mainFeed.model) {
			channels[i] = channelComponent.createObject(null,{});
			(function(channel) {
			channel.statusChanged.connect(function()  { 
				try {
					if (channel.status == XmlListModel.Ready) {
						var channelData = channel.get(0);
						if(!channelData) {
							//Workaround for atom rss feeds
							channel.namespaceDeclarations = "declare default element namespace 'http://www.w3.org/2005/Atom';";
							channelData = {};
							channelData['isAtom'] = 1;
						}
						console.log("channel :" + JSON.stringify(channelData));
						channelData['feedUrl'] = channel.source;
						loadChannelItems(channelData);
						_mainFeed.refreshing = false;
					} else if (channel.status == XmlListModel.Error){
						console.log("ERROR : Failed to load channel :" + JSON.stringify(channel));
					}
				} catch(e) {
					console.log("Failed to load channel :" + JSON.stringify(channel) + " got exception : " + e);
				}
				_mainFeed.refreshing = false;
			});
			channel.source = _mainFeed.model[i];
			})(channels[i]);
		}
	}
	
	Label {
		anchors { 
			verticalCenter:parent.verticalCenter
			left:parent.left
			right:parent.right
			margins:units.gu(2)
		}
		visible: feedList.model.count == 0 && !_mainFeed.refreshing
		text: i18n.tr("No Feeds yet... You can add feeds by clicking on the '+' sign in the top right.")
		wrapMode: Text.Wrap
	}
	
	Feed {
		id:feedList
		
		anchors {
			topMargin:header.height
			fill:parent
		}

		model: FeedsModel {
				id:feedModel
		}

		pullToRefresh {
			enabled: true
			refreshing: _mainFeed.refreshing && feedList.model.count == 0;
			onRefresh: _mainFeed.updateFeed()
		}
		
		section.property :appSettings.showSections ?  appSettings.mainFeedSectionField : ""
        section.criteria: ViewSection.FullString
        section.labelPositioning: ViewSection.InlineLabels

        section.delegate: ListItem {
            height: sectionHeader.implicitHeight + units.gu(2)
            Label {
                id: sectionHeader
                text: section
                font.weight: Font.Bold
                anchors {
                    left: parent.left
                    leftMargin: units.gu(2)
                    verticalCenter: parent.verticalCenter
                }
            }
        }
		
		delegate: FeedItem {
			onClicked:{
				mainLayout.addPageToNextColumn(	mainLayout.primaryPage, 
												Qt.resolvedUrl("EnteryViewPage.qml"),
												{"model": itemData} );
				//console.log(JSON.stringify(itemData));
			}
		}
		Timer {
			interval: appSettings.updateFeedEveryXMinutes*60000
			running: appSettings.updateFeedEveryXMinutes > 0
			repeat: true
			onTriggered: _mainFeed.updateFeed();
		}
	}
	
	BottomEdge {
		 id:bookmarksBottomEdge
		 height: parent.height
		 hint.text:i18n.tr("Bookmarks")
		 hint.deactivateTimeout:5000
		 hint.status:BottomEdgeHint.Active
		 contentComponent:	ManageBookmarksPage {
			 id:bottomBookmarks
			 opacity: bookmarksBottomEdge.dragProgress
			 implicitHeight: bookmarksBottomEdge.height
			 implicitWidth: bookmarksBottomEdge.width
		 }
	}
}

/*
 * Copyright (C) 2021  Eran DarkEye Uzan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * darkeye.ursses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

