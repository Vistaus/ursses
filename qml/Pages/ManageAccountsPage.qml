
import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2 as QControls
import Ubuntu.OnlineAccounts 2.0
import Ubuntu.Components.Popups 1.3
import Ubuntu.OnlineAccounts 2.0

import "../Components"

Page {
	id:_mainFeed
	opacity:accounts.ready ?  1 : 0.5
	enabled:accounts.ready
	header: PageHeader {
		id:header
		title:i18n.tr("Nextcloud Accounts")
		leadingActionBar {
			actions: [
				Action {
					name: i18n.tr("Back")
					iconName:"back"
					onTriggered: mainLayout.removePages(_mainFeed);
				}
			]
		}
		trailingActionBar {
			actions: [
				Action {
					name: i18n.tr("Add a new account")
					iconName:"contact-new"
					enabled:accounts.ready
					onTriggered: accounts.requestAccess(accounts.applicationId + "_nextcloud", {})
				}
			]
		}
	}
	
   //Add Online Account connection
	AccountModel {
		id: accounts
		property var host: null;
	}
	Connections {
		id: accountConnection
		target: null
		onAuthenticationReply: {
			var reply = authenticationData
			console.log(JSON.stringify(authenticationData),JSON.stringify(reply))
			if ("errorCode" in reply) {
				console.warn("Authentication error: " + reply.errorText + " (" + reply.errorCode + ")")
			} else {
				appSettings.nextCloudCreds.user = reply.Username
				appSettings.nextCloudCreds.pass = reply.Password
				appSettings.nextCloudCreds.host = accountConnection.target.settings.host
				appSettings.nextCloudCreds.accountId = accountConnection.target.accountId
				appSettings.nextCloudCreds.encodedCreds = Qt.btoa(reply.Username+":"+reply.Password)
				console.log(JSON.stringify(appSettings.nextCloudCreds));
				appSettings.nextCloudCreds = appSettings.nextCloudCreds;
			}
		}

	}
	Label {
		z:1;
		anchors {
			left:parent.left
			right:parent.right
			verticalCenter:parent.verticalCenter
			margins: units.gu(2)
		}
		visible: accounts.count === 0
		text: i18n.tr("No Nextcloud accounts available. Tap on the button at the top right to add an account.")
		wrapMode: Text.Wrap
	}
	ListView {
		anchors {
			top:header.bottom
			left:parent.left
			right:parent.right
			bottom:parent.bottom
		}
		id: accountList
		visible: accounts.count !== 0
		enabled:accounts.ready
		
		model: accounts
		
		header: Label {
			anchors {
				left:parent.left
				right:parent.right
			}
			height:units.gu(5)
			text: i18n.tr("Select an account to use")
		}
		
		delegate: ListItem {
			anchors { left: parent.left; right: parent.right }
			height: units.gu(6)
			ListItemLayout {
				title.text: model.displayName
			}
			onClicked: {
				useAccount(model.account)
			}

		}

	}
	
	//----------------------------- Functions ------------------------


	function useAccount(account) {
		accounts.host = account.settings.host
		accountConnection.target = account
		account.authenticate({host:account.settings.host,accountId:account.accountId})
	}

}

/*
 * Copyright (C) 2021  Eran DarkEye Uzan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * darkeye.ursses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

