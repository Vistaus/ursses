
import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2 as QControls

import "../Components"

Page {
	id:_mainFeed
	
	header: PageHeader {
		id:header
		title:i18n.tr("Settings")
		trailingActionBar {
			actions: [
				Action {
					name : i18n.tr("Accounts")
					iconName : "account"
					onTriggered : {
						mainLayout.addPageToNextColumn(mainLayout.primaryPage, Qt.resolvedUrl("ManageAccountsPage.qml"),{})
					}
				},
				Action {
					name : i18n.tr("Feeds")
					iconName : "view-list-symbolic"
					onTriggered : {
						mainLayout.addPageToNextColumn(mainLayout.primaryPage, Qt.resolvedUrl("ManageFeedsPage.qml"),{})
					}
				},
				Action {
					name : i18n.tr("About")
					iconName : "info"
					onTriggered : {
						mainLayout.addPageToNextColumn(mainLayout.primaryPage, Qt.resolvedUrl("Information.qml"),{})
					}
				}
			]
		}
	}
	Flickable {
		flickableDirection: Flickable.VerticalFlick
		clip:true
		anchors {
				top:header.bottom
				left:parent.left
				right:parent.right
				bottom:parent.bottom
			}
			contentHeight: settingsUI.height
			
		GenerateInputForSettings {
			id:settingsUI
			anchors {
				left:parent.left
				right:parent.right
			}
			inputsObject:appSettings
			
			fieldsSelection : {
				"mainFeedSortField" :  ["updated","channel","author","titleText"],
				"mainFeedSectionField" :  ["updated","channel","author","titleText"]
			}
			presentableNames : { return {
				showDescInsteadOfWebPage : {
					title: i18n.tr("Show content instead of linked page"),
					description :i18n.tr("Shoud we just show the feed entry content instead of navigating to the related website")
				},
				showSections : {
					title : i18n.tr("Show section name"),
					description : i18n.tr("Seperates entries by Sections")
				},
				itemsToLoadPerChannel : {
					title : i18n.tr("Item to load per feed"),
					description : i18n.tr("Please don't use too much as the sorting algoritihm is slow right now.")
				},
				mainFeedSortField : {
					title : i18n.tr("Sorting field name"),
					description : i18n.tr("What form the feed entry field will be used for sorting.")
				},
				mainFeedSectionField : {
					title : i18n.tr("Section field name"),
					description : i18n.tr(" ")
				},
				webBrowserDefaultZoom : {
					title : i18n.tr("Web browser default zoom"),
					description : i18n.tr("The zoom value to set the internal web browser to.")
				},
				updateFeedEveryXMinutes : {
					title : i18n.tr("Update feeds every"),
					description : i18n.tr("The amount of time (in minutes) to wait between automatic feeds update.")
				}
			}; }
		}
	}
}

/*
 * Copyright (C) 2021  Eran DarkEye Uzan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * darkeye.ursses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

