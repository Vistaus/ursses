import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2 as QControls
import QtWebEngine 1.7

import "../Components"

Page {
	id:_mainFeedEntryView
	property var model: {}
	
	header: PageHeader {
		id:header
		title:model.channel
		subtitle:model.titleText

		trailingActionBar {
			actions: [
				Action {
					iconName: "external-link"
					name: i18n.tr("Open in browser")
					onTriggered: {
						Qt.openUrlExternally(model.url)
					}
				},
				Action {
					iconName: "starred"
					name: i18n.tr("Save/Bookmark")
					onTriggered: {
						appSettings.bookedmarked.push(model);
						appSettings.bookedmarked = appSettings.bookedmarked;
					}
				},
				Action {
					name: i18n.tr("Share")
					enabled:false // Remove when implemented
					iconName:'share'
					onTriggered: {
						//TODO
					}
				}
			]
		}
	}
	Flickable {
		id:longDescFlick
		anchors {
			top:header.bottom
			left:parent.left
			right:parent.right
			bottom:parent.bottom
			margins:units.gu(2)
		}
		visible: appSettings.showDescInsteadOfWebPage
		clip:true
		interactive:true
		flickableDirection:Flickable.HorizontalAndVerticalFlick
		contentHeight: longDesc.height;

		Text {
			id:longDesc
			anchors {
				top:parent.top
				left:parent.left
				right:parent.right
			}
			//height:units.gu(150)
			text: model.content ? model.content : model.description.replace(/(<\s*img[^>]*>\s*)([^<])/,"$1<br/>$2")
			textFormat:Text.RichText
			wrapMode:Text.Wrap 
			color:theme.palette.normal.foregroundText
			onLinkActivated: {
				Qt.openUrlExternally(link);
			}
		}
	}
	WebEngineView {
		id: webView
		visible: !appSettings.showDescInsteadOfWebPage
		anchors {
			top:header.bottom
			left:parent.left
			right:parent.right
			bottom:parent.bottom
			margins:units.gu(0.5)
		}
		url: visible ? model.url : ""
		zoomFactor:appSettings.webBrowserDefaultZoom
	}
}

/*
 * Copyright (C) 2021  Eran DarkEye Uzan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * darkeye.ursses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

