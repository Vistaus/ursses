import QtQuick 2.4
import Ubuntu.Components 1.3

Item {
    id:__dispatcherHandler
     function handleURLs(urls) {
        if (urls && urls.length > 0) {
            for(var match in urlMapping) {
                for (var i = 0; i < urls.length; i++) {
                    if (urls[i].match(new RegExp(match))) {
                        console.log("forwarding URL : "+urls[i]);
                        urlMapping[match](urls[i]);
                    }
                }
            }
        }
    }
    
    function handleOnCompleted(args) {
                 var arguments =  (Qt.application.arguments && Qt.application.arguments.length > 0) ?
                            Qt.application.arguments :
                            (args.values.url ? [args.values.url] : []);
        __dispatcherHandler.handleURLs(arguments);
    }

    Connections {
        target: UriHandler

        onOpened: {
            console.log('Opened from UriHandler')

            if (uris.length > 0) {
                handleURLs(uris);
            }
        }
    }
}
