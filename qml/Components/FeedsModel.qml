import QtQuick 2.9
import QtQuick.XmlListModel 2.0
import QtQml.Models 2.2

ListModel {
	id:feedModel
	dynamicRoles:true
	property var sortOrder: appSettings.mainFeedSortAsc;
	property var specialSort: {
		'updated' : function(itm) {return Date.parse(itm);}
	};
	onCountChanged: sort();
	onSortOrderChanged:sort();
	//TOOD  optimize sorting
	function sort() {
		for(var i=0; i < feedModel.count;i++) {
			for(var j=i; j < feedModel.count;j++) {
				if(sortOrder !== Qt.AscendingOrder ^ feedModel.lessThen(feedModel.get(i),feedModel.get(j)) ) {
					feedModel.move(j,i,1);
					
				}
			}	
		}
	}
	// true if a is less then b
	function lessThen(a,b) {
		return 	a === undefined || a[appSettings.mainFeedSortField] === undefined || //does 'a' even exists?
				(b[appSettings.mainFeedSortField] !== undefined  && ( //well if it does it can't be less then undefined...
				( specialSort[appSettings.mainFeedSortField] && specialSort[appSettings.mainFeedSortField](a[appSettings.mainFeedSortField]) < specialSort[appSettings.mainFeedSortField](b[appSettings.mainFeedSortField]) ) //do we have a specail handleing for the selected field? 
				||
				(specialSort[appSettings.mainFeedSortField] === undefined && a[appSettings.mainFeedSortField] < b[appSettings.mainFeedSortField] ) ) );// or do we just sort it by the string value?
	}
	Component.onCompleted:sort();
}
/*
 * Copyright (C) 2021  Eran DarkEye Uzan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * darkeye.ursses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



