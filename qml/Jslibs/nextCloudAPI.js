var mapping = {
	"feed_path" : "/index.php/apps/news/api/v1-2/feeds"
}
function getFeeds(nextCloudCreds, callback) {
	if(!nextCloudCreds.host.match(/^https:/)) {
		console.log("Host is not under SSL! failing instead of exposing credentials... ");
		return false;
	}
	var creds =nextCloudCreds.encodedCreds;
	
	var xhr = new XMLHttpRequest();
	console.log(nextCloudCreds.host+mapping['feed_path']);
	console.log(nextCloudCreds);
	xhr.open("GET",nextCloudCreds.host+mapping['feed_path']);
	xhr.setRequestHeader('Authorization', 'Basic '+creds);	
	xhr.onreadystatechange = function() {
		console.log(xhr.status,xhr.readyState,xhr.responseText);
		if (xhr.readyState == 4 && xhr.status == 200) {
			callback(JSON.parse(xhr.responseText));
		}
	};
	xhr.send();
	return  true;
}
